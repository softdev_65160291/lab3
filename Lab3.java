/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author informatics
 */
import java.util.Scanner;

public class Lab3 {
    static int row;
    static int col;
    static char player = 'X';
    static int samePosition;

    static char[][] Retable = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
    static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

    static void newTable(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = Retable[i][j];
            }
            System.out.println("");
        }
    }
    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j]);

            }
            System.out.println("");
        }
    }

    static void inputPosition() {
        Scanner input = new Scanner(System.in);
        System.out.println("Turn " + player);
        System.out.println("set Row,Col :");
        row = input.nextInt() - 1;
        col = input.nextInt() - 1;
        if (table[row][col] == 'X' || table[row][col] == 'O') {
            System.out.println("Can't input same position");
            samePosition++;
        } else {
            table[row][col] = player;
        }
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static boolean checkWin() {
        for (int i = 0; i < 3;i++) {
            if (table[0][i] == player && table[1][i] == player && table[2][i] == player) {
                printTable();
                System.out.println("The winner is " + player);
                return true;
            }else if(table[i][0] == player && table[i][1] == player && table[i][2] == player) {
                printTable();
                System.out.println("The winner is " + player);
                return true;
            }
        }
        if(table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            printTable();
            System.out.println("The winner is " + player);
            return true;
        }else if(table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            printTable();
            System.out.println("The winner is " + player);
            return true;
        }else{
            return false;
        }
        
        
        
        
    }

    static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    static void printGameover() {
        System.out.println("Gameover!!");
    }

    static boolean finishedTurn() {
        System.out.println("Draw!!");
        return false;
    }

    static void gamePlay(){
        int limitTurn = 9;
        while (true) {
            samePosition = 0;
            printTable();
            inputPosition();
            if(checkWin()){
                break;
            }else{

            }
            if (samePosition > 0) {
                continue;
            }
            limitTurn--;
            if (limitTurn == 0) {
                finishedTurn();
                break;
            }
            switchPlayer();
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        samePosition = 0;

        printWelcome();
        gamePlay();

        System.out.println("New game? y/n");
        String playerChoice = input.nextLine() ;
        while(true){
            if(playerChoice.equals("y")){
                newTable();
                main(args);
            }else if(playerChoice.equals("n")){
                System.out.println("Game Over!!!");
                break;
            }else{
                System.out.println("Select again!! y/n");
                playerChoice = input.nextLine() ;
                continue;
            }
            break;
            

        }
    }
}
